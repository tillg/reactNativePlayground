# Till's ReactNativePlayground

A place to try out stuff.

## Maps (`react-native-maps`)

Trying out to get maps up and running. Was formerly known as the maps solution from AirBnB.

Steps to install the reactNativePlayground app with maps on a dev machine:

* `npm i` dugh...
* Go into the ` ios` sub directory and install pods with `pod install`
* That should be it...

Or (new way): simply run `npm run install:ios` - the npm script takes care of all.

**Watch out** As soon as you uipdate the versions of react, react-native, react-native-maps or anything involved, chances are very big, that it will not work anymore!

Stuff to read:

**[The github repo](https://github.com/react-community/react-native-maps)** of `react-native-maps`. Make sure to read the [installation instruction](https://github.com/react-community/react-native-maps/blob/master/docs/installation.md) as they are a bit special, i.e. require some extra steps.

