import React from 'react';
import { Button, Image, View, Text , StyleSheet} from 'react-native';

export default class LogoTitle extends React.Component {
    render() {
      return (
        <Image
          source={require('./till.jpg')}
          style={styles.profileImg}
        />
      );
    }
  }

const  styles = StyleSheet.create({
    profileImg: {
      height: 40,
      width: 40,
      borderRadius: 20,
    },
  });