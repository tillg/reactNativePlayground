import React from 'react';
import { Button, Image, View, StyleSheet, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import LogoTitle from './LogoTitle';
import MapView, { Marker } from 'react-native-maps';

const oceanVillaLongLat = {
    longitude: 108.280719,
    latitude: 15.975925
};
const oceanVillaMarker =
    <Marker
        coordinate={oceanVillaLongLat}
        title={'Ocean Villa'}
        description={'Our home in Vietnam'}
    />

const spicherenStrLongLat = { longitude: 11.6062079, latitude: 48.131079 };
const spicherenMarker =
    <Marker
        coordinate={spicherenStrLongLat}
        title={'Spicherenstr 12'}
        description={'Our home in Munich'}
    />

const longLatDelta = {
    latitudeDelta: 0.10922,
    longitudeDelta: 0.10421
}

export default class MapScreen extends React.Component {
    static navigationOptions = ({ navigation, navigationOptions }) => {
        //console.log(navigationOptions);
        // Notice the logs ^
        // sometimes we call with the default navigationOptions and other times
        // we call this with the previous navigationOptions that were returned from
        // this very function
        return {
            title: 'MapScreen',
            headerStyle: {
                backgroundColor: navigationOptions.headerTintColor,
            },
            headerTintColor: navigationOptions.headerStyle.backgroundColor,
        };
    };


    render() {
        return (
            <View style={styles.container}>
                <MapView style={styles.map}
                    initialRegion={{ ...spicherenStrLongLat, ...longLatDelta }}>
                    {spicherenMarker}
                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
})
